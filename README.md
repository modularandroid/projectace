# Project Ace

This project aims to standardize the most common elements in an Android Application. Each module of this project represents one of these elements.

Project Ace has therefore served as the basis for their development and contains to date all modules of the Bitbucket ModularAndroid group. 



**It is therefore highly recommended for any developer who whants to visualize how these modules are used to clone and run this project.**



To do so :

1.  Clone the project on your PC
2.  Open the ProjectAce project from android studio
3.  Under File > Project Structure > SDK Location, make sure that the path of the SDK is correct
4. Run the project on your Virtual or Physical Device.



*If you encounter an error of the type **Local sdk path not set**, close then re-open Android Studio and run the project again.*