# Gridview Module

A configurable and customizable GridView module for android.



## Installation

From Android Studio, import the library module to your project :

  1. Click File > New > Import Module

  2. Choose **gridview-module** directory and click Finish. 
     *In case of any import error, just proceed to step 3 and the error should be automatically solved.*

  3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

	```
	include ":app", ":gridview-module"   
	```

  4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

	```
	dependencies {
		implementation project(":gridview-module")
	}
	```

  5. Click Sync Project with Gradle Files (Sync Now)

  6. In your **app AndroidManifest.xml**,  under application, add this line : 

	```
	<application
		...>
		<activity android:name="com.mecreativestudio.gridview_module.GridView"/>
	</application>
	```



## Usage

You can call the module with this simple intent :

```java
Intent intent = new Intent(YOUR_ACTIVITY_NAME.this, com.mecreativestudio.gridview_module.GridView.class);


//REQUIRED
intent.putExtra("apiUrl", YOUR_VALUE);

//REQUIRED [TRUE or FALSE]
intent.putExtra("isPaginated", YOUR_VALUE); 

//IF IS PAGINATED = TRUE : REQUIRED, OPTIONAL OTHERWISE
intent.putExtra("paginationParameter", YOUR_VALUE);

//IF IS PAGINATED = TRUE : REQUIRED, OPTIONAL OTHERWISE
intent.putExtra("paginationParameterValue", YOUR_VALUE);

//IF IS PAGINATED = TRUE : REQUIRED, OPTIONAL OTHERWISE [TRUE or FALSE]
intent.putExtra("shouldPaginationParameterAutoIncrement", YOUR_VALUE);

//OPTIONAL [DEFAULT VALUE : NULL]
intent.putStringArrayListExtra("urlQueries", YOUR_ARRAY_LIST_OF_STRINGS);

//OPTIONAL [DEFAULT VALUE : 10]
intent.putExtra("maximumNumberOfItemsPerRequest", YOUR_VALUE);

//OPTIONAL [DEFAULT VALUE : 3]
intent.putExtra("numberOfItemsPerRow", YOUR_VALUE);

                
startActivity(intent);
```

*Note that you must change YOUR_ACTIVITY_NAME by the name of the activity that calls the module. Of course, you must also change YOUR_VALUE and YOUR_ARRAYLIST_OF_STRINGS.*



### 	Example

Let's say you want to populate a grid view with the data sent by this API ( https://reqres.in/).

This API returns a list of users ( avatars, first names and surnames) but you only want to display the avatars.
Having read the documentation, you know that the data is paginated and you know what queries you want to make.

In your application, you have an activity called ***MainActivity*** in which a ***gridViewButton*** button is implemented. You want your grid view to be populated and displayed after clicking this button.

By following the previous instructions, the code which you should have to do so is the following : 

```JAVA
import ...

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button gridViewButton = findViewById(R.id.gridViewButton);
        gridViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, com.mecreativestudio.gridview_module.GridView.class);

                intent.putExtra("apiUrl", "https://reqres.in/api/users");
                intent.putExtra("isPaginated", true);

                intent.putExtra("paginationParameter", "page");
                intent.putExtra("paginationParameterValue", 1); 
                intent.putExtra("shouldPaginationParameterAutoIncrement", true);

                ArrayList<String> urlQueries = new ArrayList<>();
                urlQueries.add("per_page=8");
                intent.putStringArrayListExtra("urlQueries", urlQueries); 
                
                intent.putExtra("numberOfItemsPerRow",3); 
                intent.putExtra("maximumNumberOfItemsPerRequest",8);

                startActivity(intent);
            }
        });
    }
}
```



#### Explanations

After consulting the documentation, we know that to retrieve the users list, the URL API is: https://reqres.in/api/users
The data being paginated, the variable *isPaginated* is thus equal to *true*. 

According to the documentation, the *paginationParameter* is *page*. 
In this case, we want to start at page 1 ( *paginationParameterValue = 1*) and want to scan all API pages ( *shouldPaginationParameterAutoIncrement = true* ).

Still according to the documentation, we want to get 8 users avatars per page ( *per_page = 8* ).

We want to make a request to the next page when we have retrieved the 8 users avatars of the current page. 
Therefore, we set the maximum number of items per request to 8 ( *maximumNumberOfItemsPerRequest = 8* ). 
Thus, when 8 users avatars have been retrieved, a request to the next page will be made.

Finally, we want to display 3 items / users avatars per row ( *numberOfItemsPerRow = 3* )



## Configuration and Customization

This GridView Module is fully configurable and customizable.  

If you want to adapt its behavior to your needs, you can simply edit the GridView and Adapter Java classes which are located here :

```
YOUR_PROJECT/gridview-module/java/com.mecreativestudio.gridview_module/GridView.java

YOUR_PROJECT/gridview-module/java/com.mecreativestudio.gridview_module/Adapter.java
```



#### Guideline	

1.   In the GridView Java Class : 

	 - Adapt the JSON Array (line 229) to your case

	 - Adapt the Item class

	 - Adapt the initArrayLists method

	 - Adapt the populateItem method



2.   In the Adapter Java Class :

	 - Adapt the variables (line 20) to your case

	 - Adapt the 4 methods below the line 58



*Important : These files have been optimized for case-by-case use. It will therefore not be necessary for you to modify the parts of the code not mentioned above.*



If you want to edit its layout, you can simply edit the activity_recycler_view.xml file which is located here :

```xml
YOUR_PROJECT/gridview-module/res/layout/activity_grid_view.xml
```



If you want to edit the layout of a single item, you can simply edit the item.xml file which is located here :

```
YOUR_PROJECT/gridview-module/res/layout/item.xml
```

*Note that in this case, it is also essential to modify the resizeImages method (at line 46) of the Adapter.java class mentioned above.*