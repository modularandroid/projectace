package com.testexample.qr_code_reader_module;

import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

public class QrCodeReader extends AppCompatActivity {

    private  QRCodeReaderView qrCodeReaderView;
    private boolean flashLightState = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_code_reader);

        qrCodeReaderView = findViewById(R.id.qrDecoderView);
        qrCodeReaderView.setOnQRCodeReadListener(new QRCodeReaderView.OnQRCodeReadListener() {
            // Fetch data from the Qr Code and do something
            @Override
            public void onQRCodeRead(String text, PointF[] points) {
                // Do something
                /*Toast.makeText(QrCodeReader.this, text, Toast.LENGTH_SHORT).show();*/
            }
        });

        // Use this function to enable/disable decoding
        qrCodeReaderView.setQRDecodingEnabled(true);

        // Use this function to change the autofocus interval (default is 5 secs)
        qrCodeReaderView.setAutofocusInterval(2000L);

        // Function that enable/disable Torch
        TextView textView = findViewById(R.id.textView);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flashLightState){
                    qrCodeReaderView.setTorchEnabled(false);
                    flashLightState = false;
                }else{
                    qrCodeReaderView.setTorchEnabled(true);
                    flashLightState = true;
                }
            }
        });

        // Use this function to set front camera preview
        qrCodeReaderView.setFrontCamera();

        // Use this function to set back camera preview
        qrCodeReaderView.setBackCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();
        qrCodeReaderView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }
}
