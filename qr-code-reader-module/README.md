# Qr Code Reader Module

A configurable and customizable Qr Code Reader module for android.



## Requirements

You must add the maven repository to your project. To do so, from Android Studio in your **root gradle file, **add the following line: 

```
allprojects {
     repositories {
		...
        maven { url 'https://jitpack.io' }
	}
}
```



## Installation

From Android Studio, import the library module to your project :

  1. Click File > New > Import Module

  2. Choose **qr-code-reader-module** directory and click Finish.
     *In case of any import error, just proceed to step 3 and it should be automatically solved.*

  3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

     ```
     include ':app', ':qr-code-reader-module'   
     ```

4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

   ```
   dependencies {
   	implementation project(":qr-code-reader-module")
   }
   ```

5. Click Sync Project with Gradle Files (Sync Now)

6. In your **app AndroidManifest.xml**, add the following permissions :

   ```
   <uses-permission android:name="android.permission.CAMERA" />
   <uses-permission android:name="android.permission.VIBRATE" />
   <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
   <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
   <uses-feature android:name="android.hardware.camera" />
   <uses-feature android:name="android.hardware.camera.autofocus" android:required="false" />
   ```

   In the same file, under application, add this line : 

   ```
   <application
   ...>
   	<activity android:name="com.testexample.qr_code_reader_module.QrCodeReader" /></application>
   ```



## Usage

In the activity in which you want to call the module, import it by adding the following line :

```
import com.testexample.qr_code_reader_module.QrCodeReader;
```



Before any call to this module, it is essential that the camera permissions are granted. In order to verify this, you can use the following code : 

```
//Check for camera permission
if (ContextCompat.checkSelfPermission(YOUR_ACTIVITY_NAME.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

	//Request for granted the camera permission
	ActivityCompat.requestPermissions(YOUR_ACTIVITY_NAME.this, new String[{Manifest.permission.CAMERA}, 102);
	
}
```

*Note that you must change YOUR_ACTIVITY_NAME by the name of the activity that calls the module.*



When camera permissions are granted, you can call this module with a simple intent :

```
Intent intent = new Intent(YOUR_ACTIVITY_NAME.this, QrCodeReader.class);
startActivity(intent);
```



The Qr Code reading result can be managed in the QrCodeReader.java class *(cf. Configuration and Customization)*, and especially in this part of the code :

```
qrCodeReaderView = findViewById(R.id.qrDecoderView);
qrCodeReaderView.setOnQRCodeReadListener(new QRCodeReaderView.OnQRCodeReadListener() {

    // Fetch data from the Qr Code and do something
    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        // Do something       
    }
    
});
```



### 	Example

Let's say that your application contains an activity, called ***MainActivity***, in which a ***qrCodeReaderButton*** button is implemented.
You want the Qr Code Reader Module to be called when you click on this button. 

By following the previous instructions, the code which you should have to do so is the following : 

> ```
> import ...
> import com.testexample.qr_code_reader_module.QrCodeReader;
> 
> public class MainActivity extends AppCompatActivity {
> 
>     @Override
>     protected void onCreate(Bundle savedInstanceState) {
>         super.onCreate(savedInstanceState);
>         setContentView(R.layout.activity_main);
> 
>         //Check for camera permission
>         if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
>             //Request for granted the camera permission
>             ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 102);
>         }
> 
>         //Add a click listener on a button
>         Button qrCodeReaderButton = findViewById(R.id.qrCodeReaderButton);
>         qrCodeReaderButton.setOnClickListener(new View.OnClickListener() {
>             @Override
>             public void onClick(View v) {
>                 //When a click is made on this button, open the QrCodeReader Module
>                 Intent intent = new Intent(MainActivity.this, QrCodeReader.class);
>                 startActivity(intent);
>             }
>         });
>     }
> }
> ```



## Configuration and Customization

This Qr Code Reader Module is fully configurable and customizable.  

If you want to adapt its behavior to your needs, you can simply edit the QrCodeReader Java class which is located here :

```
YOUR_PROJECT/qr-code-reader-module/src/main/java/com.testexample.qr_code_reader_module/QrCodeReader.java
```



If you want to edit its layout, you can simply edit the qr_code_reader.xml file that is located here :

```
YOUR_PROJECT/qr-code-reader-module/src/main/res/layout/qr_code_reader.xml
```

