package com.mecreativestudio.search_module;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private ArrayList<String> name = new ArrayList<>();
    private ArrayList<String> username = new ArrayList<>();
    private ArrayList<String> email = new ArrayList<>();
    private Context mContext;

    View view;

    public SearchAdapter(ArrayList<String> name, ArrayList<String> username, ArrayList<String> email, Context mContext) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.mContext = mContext;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_result_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        holder.name.setText(name.get(position));
        holder.username.setText(username.get(position));
        holder.email.setText(email.get(position));

        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("USER INFO", holder.name.getText().toString() + " " + holder.username.getText().toString() + " " + holder.email.getText().toString());
            }
        });

    }

    @Override
    public int getItemCount() {
        return name.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ConstraintLayout constraintLayout;
        TextView name;
        TextView username;
        TextView email;

        public ViewHolder(View itemView) {
            super(itemView);

            this.constraintLayout = itemView.findViewById(R.id.constraintLayout);
            this.name = itemView.findViewById(R.id.nameView);
            this.username = itemView.findViewById(R.id.usernameView);
            this.email = itemView.findViewById(R.id.emailView);
        }
    }
}
