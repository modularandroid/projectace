package com.mecreativestudio.search_module;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;

public class SearchModule extends Activity {

    private EditText searchInput;
    private RecyclerView recyclerView;
    private TextView noResultView;

    private SearchAdapter adapter;
    private LinearLayoutManager linearLayoutManager;


    //TODO : STEP 1 : ADAPT THE 4 VARIABLES BELOW TO YOUR CASE
    private ArrayList<String> names;
    private ArrayList<String> usernames;
    private ArrayList<String> emails;
    private String url = "https://jsonplaceholder.typicode.com/users";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_layout);

        searchInput = findViewById(R.id.searchInput);

        //Hide the recycler view at the beginning
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setVisibility(View.INVISIBLE);

        noResultView = findViewById(R.id.noResultView);

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.toString().length() > 3){
                    initArrayLists();
                    String finalUrl = url +"?q=" + charSequence.toString();
                    getData(finalUrl);

                }else{
                    recyclerView.setVisibility(View.INVISIBLE);
                    noResultView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        initArrayLists();
        initRecyclerView();
    }

    private void initRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    //TODO : STEP 2 : ADAPT THE JSON ARRAY RESPONSE TREATMENT TO YOUR CASE IN THE getData() AND setData() METHODS
    private void getData(final String URL) {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    OkHttpClient client = new OkHttpClient();

                    okhttp3.Request request;
                    request = new okhttp3.Request.Builder()
                            .header("Content-Type", "content-type/application/json;")
                            .url(URL)
                            .get()
                            .build();

                    final okhttp3.Response response = client.newCall(request).execute();
                    if (response.isSuccessful()) {

                        runOnUiThread( new Runnable(){
                            @Override

                            public void run() {
                                try {
                                    setData((new JSONArray(response.body().string())));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                initRecyclerView();

                            }
                        });
                    }else{
                        //Handle error
                    }
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private void setData(JSONArray response){
        //Clear the array lists from previous search
        clearArrayLists();

        try {

            if(response.length() > 0 ){
                for (int i = 0; i < response.length() ; i++) {
                    JSONObject data = response.getJSONObject(i);

                    names.add(String.valueOf(data.get("name")));
                    usernames.add(String.valueOf(data.get("username")));
                    emails.add(String.valueOf(data.get("email")));
                }
                recyclerView.setVisibility(View.VISIBLE);
                noResultView.setVisibility(View.INVISIBLE);
            }else{
                recyclerView.setVisibility(View.INVISIBLE);
                noResultView.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    //TODO : STEP 3 : ADAPT THE initArrayLists() AND clearArrayLists() METHODS TO YOUR CASE
    private void initArrayLists() {
        names = new ArrayList<>();
        usernames = new ArrayList<>();
        emails = new ArrayList<>();

        adapter = new SearchAdapter(names, usernames, emails,this);
    }

    private void clearArrayLists(){
        names.clear();
        usernames.clear();
        emails.clear();
    }

}
