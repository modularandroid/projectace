package com.testexample.projectAce;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mecreativestudio.calendar_view_module.CalendarViewActivity;
import com.mecreativestudio.dialog_module.DialogModule;
import com.mecreativestudio.gridview_inflater_module.GridViewInflater;
import com.mecreativestudio.projectace.recyclerviewmodule.ReyclerView;
import com.mecreativestudio.qr_code_generator_module.QrCodeGenerator;
import com.mecreativestudio.search_module.SearchModule;
import com.mecreativestudio.select_image_module.SelectImage;
import com.mecreativestudio.step_counter_module.StepCounter;
import com.mecreativestudio.view_image_module.ViewImage;
import com.mecreativestudio.view_multiple_images_module.ViewMultipleImages;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.testexample.qr_code_reader_module.QrCodeReader;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    int linearLayoutPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Check for camera permission
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //Request for granted the camera permission
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 102);
        }

        //Add a click listener on the qrCodeReaderButton
        Button qrCodeReaderButton = findViewById(R.id.qrCodeReaderButton);
        qrCodeReaderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the QrCodeReader Module
                Intent intent = new Intent(MainActivity.this, QrCodeReader.class);
                startActivity(intent);
            }
        });

        //Add a click listener on the qrCodeReaderButton
        Button selectImageButton = findViewById(R.id.selectImageButton);
        selectImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the SelectImage Module
                Intent intent = new Intent(MainActivity.this, SelectImage.class);
                startActivity(intent);
            }
        });

        //Add a click listener on the viewImageButton
        Button viewImageButton = findViewById(R.id.viewImageButton);
        viewImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the ViewImage Module
                Intent intent = new Intent(MainActivity.this, ViewImage.class);

                //View image from its url
                intent.putExtra("url", "https://images.pexels.com/photos/1324500/pexels-photo-1324500.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260");
                startActivity(intent);

                //View image from its associated file
                //intent.putExtra("file", new File("/storage/emulated/0/DCIM/20180816_152522630_341171469f8418737b92af29ce95cc4e.jpg"));
                //startActivity(intent);
            }
        });

        //Add a click listener on the viewMultipleImagesButton
        Button viewMultipleImagesButton  = findViewById(R.id.viewMultipleImagesButton);
        viewMultipleImagesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the viewMultipleImages Module
                Intent intent = new Intent(MainActivity.this, ViewMultipleImages.class);

                //View images from their urls
                ArrayList<String> imageUrls = new ArrayList<>();
                imageUrls.add("http://cdn05.branchez-vous.com/wp-content/uploads/2016/09/bge2-800x410.jpg");
                imageUrls.add("https://tinypng.com/images/social/website.jpg");
                intent.putStringArrayListExtra("imageUrls", imageUrls);
                startActivity(intent);

                //View images from their associated files
//                ArrayList<File> imageFiles = new ArrayList<>();
//                imageFiles.add(new File("/storage/emulated/0/DCIM/20180816_152522630_341171469f8418737b92af29ce95cc4e.jpg"));
//                imageFiles.add(new File("/storage/emulated/0/DCIM/20180816_152530746_ee23e92e54fc93a496795dcf0cfdf70a.jpg"));
//                intent.putExtra("imageFiles", imageFiles);
//                startActivity(intent);
            }
        });

        Button recyclerViewButton = findViewById(R.id.recyclerViewButton);
        recyclerViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, ReyclerView.class);

                intent.putExtra("apiUrl", "https://reqres.in/api/users"); //REQUIRED
                intent.putExtra("isPaginated", true); //REQUIRED [TRUE or FALSE]

                intent.putExtra("paginationParameter", "page"); //REQUIRED IF IS PAGINATED = TRUE, OPTIONAL OTHERWISE
                intent.putExtra("paginationParameterValue", 1); //REQUIRED IF IS PAGINATED = TRUE, OPTIONAL OTHERWISE
                intent.putExtra("shouldPaginationParameterAutoIncrement", true); //REQUIRED IF IS PAGINATED = TRUE, OPTIONAL OTHERWISE

                ArrayList<String> urlQueries = new ArrayList<>();
                urlQueries.add("per_page=8");
                intent.putStringArrayListExtra("urlQueries", urlQueries); //OPTIONAL [DEFAULT VALUE : NULL]

                intent.putExtra("maximumNumberOfItemsPerRequest",8); //OPTIONAL [DEFAULT VALUE : 10]
                intent.putExtra("numberOfItemsPerRow",1); //OPTIONAL [DEFAULT VALUE : 1]

                startActivity(intent);
            }
        });

        Button gridViewButton = findViewById(R.id.gridViewButton);
        gridViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, com.mecreativestudio.gridview_module.GridView.class);

                intent.putExtra("apiUrl", "https://reqres.in/api/users"); //REQUIRED
                intent.putExtra("isPaginated", true); //REQUIRED [TRUE or FALSE]

                intent.putExtra("paginationParameter", "page"); //REQUIRED IF IS PAGINATED = TRUE, OPTIONAL OTHERWISE
                intent.putExtra("paginationParameterValue", 1); //REQUIRED IF IS PAGINATED = TRUE, OPTIONAL OTHERWISE
                intent.putExtra("shouldPaginationParameterAutoIncrement", true); //REQUIRED IF IS PAGINATED = TRUE, OPTIONAL OTHERWISE

                ArrayList<String> urlQueries = new ArrayList<>();
                urlQueries.add("per_page=8");
                intent.putStringArrayListExtra("urlQueries", urlQueries); //OPTIONAL [DEFAULT VALUE : NULL]
                intent.putExtra("numberOfItemsPerRow",3); //OPTIONAL [DEFAULT VALUE : 3]
                intent.putExtra("maximumNumberOfItemsPerRequest",8); //OPTIONAL [DEFAULT VALUE : 10]

                startActivity(intent);
            }
        });

        /*---------------------------------------------------------------------------------*/
        /*------------------------------GRIDVIEW INFLATER----------------------------------*/
        /*---------------------------------------------------------------------------------*/

        //Find the linearLayout and inflate it with the gridView
        final LinearLayout myLinearLayout = findViewById(R.id.myLinearLayout);
        LayoutInflater.from(this).inflate(com.mecreativestudio.gridview_inflater_module.R.layout.activity_grid_view, myLinearLayout);

        //Get the vertical position of the linearLayout
        myLinearLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(){
            @Override public void onGlobalLayout(){
                int [] location = new int[2];
                myLinearLayout.getLocationOnScreen(location);
                linearLayoutPosition = location[1];
                myLinearLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);

            }
        });

        //Find the recyclerView of the GridView and disabled its own scrolling
        final RecyclerView recyclerView = findViewById(com.mecreativestudio.gridview_inflater_module.R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(false);

        //Find the scrollView of the activity and adding an onScrollListener on it
        final ScrollView myScrollView = findViewById(R.id.myScrollView);
        myScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                //Get the current position of the vertical scroll
                int scrollY = myScrollView.getScrollY();
                //If the vertical scroll is equal or greater than the linear layout position (means that the linearLayout is on the top of the screen)
                if(scrollY >= linearLayoutPosition){
                    //Enabled the scrolling inside the recycler view
                    recyclerView.setNestedScrollingEnabled(true);
                }else{
                    //Else, disabled it again
                    recyclerView.setNestedScrollingEnabled(false);
                }
            }
        });

        ArrayList<String> urlQueries = new ArrayList<>();
        urlQueries.add("per_page=8");

        new GridViewInflater("https://reqres.in/api/users", true, "page", 1, true, urlQueries, 4, 8, recyclerView);

        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/


        /*---------------------------------------------------------------------------------*/
        /*------------------------------ACTIVITY INDICATOR---------------------------------*/
        /*---------------------------------------------------------------------------------*/
        final Button activityIndicatorButton = findViewById(R.id.activityIndicatorButton);
        final ProgressBar activityIndicator = findViewById(R.id.activityIndicator);

        activityIndicatorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityIndicator.setVisibility(View.VISIBLE);
                activityIndicatorButton.setVisibility(View.INVISIBLE);
            }
        });

        activityIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityIndicator.setVisibility(View.INVISIBLE);
                activityIndicatorButton.setVisibility(View.VISIBLE);
            }
        });
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/

        //Add a click listener on the qrCodeGeneratorButton
        Button qrCodeGeneratorButton = findViewById(R.id.qrCodeGeneratorButton);
        qrCodeGeneratorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the qr-code-generator-module
                Intent intent = new Intent(MainActivity.this, QrCodeGenerator.class);
                intent.putExtra("value", "Hello world !");
                intent.putExtra("dimension", 600);
                startActivity(intent);
            }
        });

        //Add a click listener on the calendarViewButton
        Button calendarViewButton = findViewById(R.id.calendarViewButton);
        calendarViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the calendar-view-module
                Intent intent = new Intent(MainActivity.this, CalendarViewActivity.class);
                startActivity(intent);
            }
        });


        /*---------------------------------------------------------------------------------*/
        /*------------------------------STEPCOUNTER LAYOUT---------------------------------*/
        /*---------------------------------------------------------------------------------*/
        //Find the linearLayout and inflate it with the stepcounter
        final LinearLayout stepCounterLayout = findViewById(R.id.stepCounterLayout);
        LayoutInflater.from(this).inflate(com.mecreativestudio.step_counter_module.R.layout.step_counter, stepCounterLayout);

        //Find the textView of the step-counter-module
        final TextView stepCounterValue = findViewById(com.mecreativestudio.step_counter_module.R.id.stepCounterValue);

        new StepCounter(this, stepCounterValue);
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/


        /*---------------------------------------------------------------------------------*/
        /*--------------------------------SHARING SNIPPET----------------------------------*/
        /*---------------------------------------------------------------------------------*/
        //Add a click listener on the shareHTMLButton
        Button shareHTMLButton = findViewById(R.id.shareHTMLButton);
        shareHTMLButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, share HTML
                String HTMLToShare = "" +
                        "<h3>Title of the text shared</h3>" +
                        "<p>This is the HTML text shared.</p>";

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(HTMLToShare));

                startActivity(Intent.createChooser(intent, "Share using HTML Text"));
            }
        });

        //Add a click listener on the shareImageButton
        Button shareImageButton = findViewById(R.id.shareImageButton);
        shareImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //When a click is made on this button, share Image
                File photoFile = new File(getFilesDir(), "09-posterization-opt.jpg");

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(photoFile));
                intent.setType("image/*");

                startActivity(Intent.createChooser(intent, "Share Image and Text"));
            }
        });
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/


        /*---------------------------------------------------------------------------------*/
        /*----------------------------------DIALOG MODULE----------------------------------*/
        /*---------------------------------------------------------------------------------*/
        //Add a click listener on the dialogButton
        Button dialogButton = findViewById(R.id.dialogButton);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Get the fragment manager
                FragmentManager fragmentManager = getSupportFragmentManager();
                //Create a new instance of dialogModule
                DialogModule dialogModule = new DialogModule();
                //Avoid user to exit dialog module by clicking outside
                dialogModule.setCancelable(false);
                //Display dialog module
                dialogModule.show(fragmentManager, "dialogModule");
            }
        });
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/


        /*---------------------------------------------------------------------------------*/
        /*----------------------------------SEARCH MODULE----------------------------------*/
        /*---------------------------------------------------------------------------------*/
        //Add a click listener on the searchButton
        Button searchButton = findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the search-module
                Intent intent = new Intent(MainActivity.this, SearchModule.class);
                startActivity(intent);
            }
        });
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------------------*/

        Toolbar myToolbar = findViewById(R.id.myToolbar);
        myToolbar.bringToFront();
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(" ");

        NavigationDrawerBuilder(this, myToolbar);
    }

    private void NavigationDrawerBuilder(Context context, Toolbar toolbar){

        final Activity activity = (Activity) context;

        //Define the items inside the drawer
        PrimaryDrawerItem selectImageItem = new PrimaryDrawerItem().withIdentifier(1).withName("Select Image");
        PrimaryDrawerItem myScreenItem = new PrimaryDrawerItem().withIdentifier(2).withName("My Screen");
        PrimaryDrawerItem calendarViewItem = new PrimaryDrawerItem().withIdentifier(3).withName("My Calendar");

        //Create the Header
        AccountHeader drawerHeader = new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.ace)
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withAccountHeader(drawerHeader)
                .withActivity(activity)
                .withToolbar(toolbar)
                .addDrawerItems(
                        selectImageItem,
                        new DividerDrawerItem(),
                        myScreenItem,
                        new DividerDrawerItem(),
                        calendarViewItem

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        switch ((int) drawerItem.getIdentifier()){
                            case 1:
                                startActivity(new Intent(activity, SelectImage.class));
                                return true;
                            case 2:
                                startActivity(new Intent(activity, MyScreen.class));
                                return true;
                            case 3:
                                startActivity(new Intent(activity, CalendarViewActivity.class));
                                return true;
                        }
                        return true;
                    }
                })
                .build();
    }
}
