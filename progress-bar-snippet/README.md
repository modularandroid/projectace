# Progress Bar Snippet

The purpose of this documentation is to provide an overview about the typical use cases of the Android  Progress Bar (Activity Indicator for IOS / React Native).



## Requirements

For this entire documentation, we will impose the use of ConstraintLayouts (allowing to create complex and responsive layouts).

From Android Studio, open the **app gradle file** and add these two lines to the dependencies block as shown here :

```java
implementation "com.android.support:appcompat-v7:28.0.0-rc01"
implementation "com.android.support.constraint:constraint-layout:1.1.2"
```



## Use Case 1 : Full-Screen Display

Let's say you have an activity containing several components (LinearLayout, View, Button, TextView, etc. ). You have a task to perform and want to display, in full-screen, a Progress Bar until this task is finished.

For our example we want to do this in the ***MainActivity*** activity which has ***activity_main*** as layout.



The layout ( *activity_main.xml* ) should be similar to this one :

```xml
<android.support.constraint.ConstraintLayout
	xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity" >    

		<Component1
            android:id="@+id/component1"
            android:layout_width="..."
            android:layout_height="..."
			... />

        <Component2
            android:id="@+id/component2"
            android:layout_width="..."
            android:layout_height="..."
			... />

        <Component3
            android:id="@+id/component3"
            android:layout_width="..."
            android:layout_height="..."
			... />

        <ProgressBar
            android:id="@+id/progressBar"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginBottom="8dp"
            android:layout_marginEnd="8dp"
            android:layout_marginLeft="8dp"
            android:layout_marginRight="8dp"
            android:layout_marginStart="8dp"
            android:indeterminateOnly="true"
            android:keepScreenOn="true"
            android:visibility="invisible"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
                
</android.support.constraint.ConstraintLayout>
```



The associated java class ( MainActivity.java) should be similar to this one :

```java
import ...

public class MainActivity extends AppCompatActivity {

    ComponentType component1;
    ComponentType component3;
    ComponentType component3;
    ProgressBar progressBar;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        component1 = findViewById(R.id.component1);
        component2 = findViewById(R.id.component2);
        component3 = findViewById(R.id.component3);
        
        //Hide the progress bar
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
    }
    
    .
    myTask();
    .
    
    private void myTask(){
        //Display the progress bar and hide all the others components of the activity
        progressBar.setVisibility(View.VISIBLE);
        component1.setVisibility(View.INVISIBLE);
        component2.setVisibility(View.INVISIBLE);
        component3.setVisibility(View.INVISIBLE);
        
        /*START OF THE TASK*/
        //DO YOUR TASK
        /*END OF TASK*/
        
        //Hide the progress bar and display all the others components of the activity
        progressBar.setVisibility(View.INVISIBLE);
        component1.setVisibility(View.VISIBLE);
        component2.setVisibility(View.VISIBLE);
        component3.setVisibility(View.VISIBLE);
    }
```

*In the case of an AsyncTask, you can place lines 30 to 33 in the onPreExecute() method and lines 40 to 43 in the onPostExecute() method*

*If you want the Progress Bar to be directly visible at the start of your activity and only disappear after you have completed your task, replace line 21 with lines 30 to 33.*



## Use Case 2 : Partial-Screen Display

Let's say you have an activity containing several components (LinearLayout, View, Button, TextView, etc. ). You have a task to perform and want to display, in partial-screen, a Progress Bar until this task is finished.

Progress bar can be displayed before/after a component or between two components

For our example we want to do this in the ***MainActivity*** activity which has ***activity_main*** as layout and displayed the Progress bar between ***component1*** and ***component2***.



The layout ( *activity_main.xml* ) should be similar to this one :

```xml
<android.support.constraint.ConstraintLayout
	xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity" >    

		<Component1
            android:id="@+id/component1"
            android:layout_width="..."
            android:layout_height="..."
			... />

        <Component2
            android:id="@+id/component2"
            android:layout_width="..."
            android:layout_height="..."
			... />

        <Component3
            android:id="@+id/component3"
            android:layout_width="..."
            android:layout_height="..."
			... />

        <ProgressBar
            android:id="@+id/progressBar"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginBottom="8dp"
            android:layout_marginEnd="8dp"
            android:layout_marginLeft="8dp"
            android:layout_marginRight="8dp"
            android:layout_marginStart="8dp"
            android:indeterminateOnly="true"
            android:keepScreenOn="true"
            android:visibility="invisible"
                
            <!--This line has changed-->
            app:layout_constraintBottom_toTopOf="component2"
                
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
                
            <!--This line has changed-->
            app:layout_constraintTop_toBottomOf="component1" />
                
</android.support.constraint.ConstraintLayout>
```



The associated java class ( MainActivity.java) should be similar to this one :

```java
import ...

public class MainActivity extends AppCompatActivity {

    ComponentType component1;
    ComponentType component3;
    ComponentType component3;
    ProgressBar progressBar;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        component1 = findViewById(R.id.component1);
        component2 = findViewById(R.id.component2);
        component3 = findViewById(R.id.component3);
        
        //Hide the progress bar
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
    }
    
    .
    myTask();
    .
    
    private void myTask(){
        //Display the progress bar
        progressBar.setVisibility(View.VISIBLE);
        
        /*START OF THE TASK*/
        //DO YOUR TASK
        /*END OF TASK*/
        
        //Hide the progress bar
        progressBar.setVisibility(View.INVISIBLE);
    }
```

*In the case of an AsyncTask, you can place line 30 in the onPreExecute() method and line 37 in the onPostExecute() method*

*If you want the Progress Bar to be directly visible at the start of your activity and only disappear after you have completed your task, replace line 21 with line 30*



## Use Case 3 : Inside a Component Display

This type of use case is particularly appreciated in RecyclerView : the Progress bar is visible when new data is loaded.

Let's say you have an activity containing several components (LinearLayout, View, Button, TextView, etc. ). You have a task to perform and want to display, inside a component, a Progress Bar until this task is finished.

For our example we want to do this in the ***MainActivity*** activity which has ***activity_main*** as layout and display the Progress bar inside the ***component2***.



The layout ( *activity_main.xml* ) should be similar to this one :

```xml
<android.support.constraint.ConstraintLayout
	xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity" >    

		<Component1
            android:id="@+id/component1"
            android:layout_width="..."
            android:layout_height="..."
			... />

        <Component2
            android:id="@+id/component2"
            android:layout_width="..."
            android:layout_height="..."
			... >
                
                <ProgressBar
                    android:id="@+id/progressBar"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginBottom="8dp"
                    android:layout_marginEnd="8dp"
                    android:layout_marginLeft="8dp"
                    android:layout_marginRight="8dp"
                    android:layout_marginStart="8dp"
                    android:indeterminateOnly="true"
                    android:keepScreenOn="true"
                    android:visibility="invisible"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintBottom_toBottomOf="parent" />
                        
        </Component2>

        <Component3
            android:id="@+id/component3"
            android:layout_width="..."
            android:layout_height="..."
			... />
                
</android.support.constraint.ConstraintLayout>
```



The associated java class ( MainActivity.java) should be similar to this one :The associated java class ( MainActivity.java) should be similar to this one :

```java
import ...

public class MainActivity extends AppCompatActivity {

    ComponentType component1;
    ComponentType component3;
    ComponentType component3;
    ProgressBar progressBar;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        component1 = findViewById(R.id.component1);
        component2 = findViewById(R.id.component2);
        component3 = findViewById(R.id.component3);
        
        //Hide the progress bar
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
    }
    
    .
    myTask();
    .
    
    private void myTask(){
        //Display the progress bar
        progressBar.setVisibility(View.VISIBLE);
        
        /*START OF THE TASK*/
        //DO YOUR TASK
        /*END OF TASK*/
        
        //Hide the progress bar
        progressBar.setVisibility(View.INVISIBLE);
    }
```

*In the case of an AsyncTask, you can place line 30 in the onPreExecute() method and line 37 in the onPostExecute() method*

*If you want the Progress Bar to be directly visible at the start of your activity and only disappear after you have completed your task, replace line 21 with line 30*



## Use Case 4 : Instead of a Component Display

Let's say you have an activity containing several components (LinearLayout, View, Button, TextView, etc. ). You have a task to perform and want to display, instead of a component, a Progress Bar until this task is finished.

For our example we want to do this in the ***MainActivity*** activity which has ***activity_main*** as layout and display the Progress bar instead of the ***component2***.



The layout ( *activity_main.xml* ) should be similar to this one :

```xml
<android.support.constraint.ConstraintLayout
	xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity" >    

		<Component1
            android:id="@+id/component1"
            android:layout_width="..."
            android:layout_height="..."
			... />

        <Component2
            android:id="@+id/component2"
            android:layout_width="..."
            android:layout_height="..."
            ...
            <!--These lines have changed-->
            app:layout_constraintBottom_toTopOf="component3"
			app:layout_constraintTop_toBottomOf="component1" />

        <Component3
            android:id="@+id/component3"
            android:layout_width="..."
            android:layout_height="..."
			... />

        <ProgressBar
            android:id="@+id/progressBar"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginBottom="8dp"
            android:layout_marginEnd="8dp"
            android:layout_marginLeft="8dp"
            android:layout_marginRight="8dp"
            android:layout_marginStart="8dp"
            android:indeterminateOnly="true"
            android:keepScreenOn="true"
            android:visibility="invisible"
                
            <!--This line has changed-->
            app:layout_constraintBottom_toTopOf="component3"
                
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
                
            <!--This line has changed-->
            app:layout_constraintTop_toBottomOf="component1" />
                
</android.support.constraint.ConstraintLayout>
```



The associated java class ( MainActivity.java) should be similar to this one :

```java
import ...

public class MainActivity extends AppCompatActivity {

    ComponentType component1;
    ComponentType component3;
    ComponentType component3;
    ProgressBar progressBar;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        component1 = findViewById(R.id.component1);
        component2 = findViewById(R.id.component2);
        component3 = findViewById(R.id.component3);
        
        //Hide the progress bar
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
    }
    
    .
    myTask();
    .
    
    private void myTask(){
        //Display the progress bar and hide the component2
        progressBar.setVisibility(View.VISIBLE);
        component2.setVisibility(View.INVISIBLE);
        
        /*START OF THE TASK*/
        //DO YOUR TASK
        /*END OF TASK*/
        
        //Hide the progress bar and display the component2
        progressBar.setVisibility(View.INVISIBLE);
        component2.setVisibility(View.VISIBLE);
    }
```

*In the case of an AsyncTask, you can place lines 30 to 31 in the onPreExecute() method and lines 38 to 39 in the onPostExecute() method*

*If you want the Progress Bar to be directly visible at the start of your activity and only disappear after you have completed your task, replace line 21 with lines 30 to 31.*