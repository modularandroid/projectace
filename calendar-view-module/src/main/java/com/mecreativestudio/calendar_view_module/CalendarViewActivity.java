package com.mecreativestudio.calendar_view_module;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

public class CalendarViewActivity extends AppCompatActivity {

    private Calendar calendar;

    private MaterialCalendarView calendarView;
    private ImageButton editEventButton;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private String[] eventsDates;
    private ArrayList<String> events = new ArrayList<>();
    private String year;
    private String month;
    private String day;
    private SimpleDateFormat simpleDateFormat;
    private String todayDate;
    Date date;
    private DayDecorator dayDecorator;

    private FileInputStream fileInputStream;
    private InputStreamReader inputStreamReader;
    private BufferedReader bufferedReader;
    private StringBuilder stringBuilder;
    private String eventLine;

    private String selectedDate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_view_layout);

        //Get the events dates from the internal storage
        eventsDates = fileList();

        calendarView = findViewById(R.id.calendarView);
        editEventButton = findViewById(R.id.editEventButton);
        recyclerView = findViewById(R.id.recyclerView);

        initRecyclerView();

        //Add a click listener on the calendarViewButton
        editEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Intent to EditEvent. We precise that the user want to create a event for the selected date
                Intent intent = new Intent(CalendarViewActivity.this, EditEvent.class);
                intent.putExtra("createEvent", true);
                if(selectedDate != null){
                    intent.putExtra("dateReceived", selectedDate);
                }else{
                    intent.putExtra("dateReceived", todayDate);
                }
                startActivity(intent);
            }
        });

        calendarView.setOnDateChangedListener(new OnDateSelectedListener(){
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                selectedDate = treatDate(calendarDay);
                readEvents(selectedDate, events);
            }
        });

        initCalendar();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        calendarView.removeDecorators();
        eventsDates = fileList();
        initRecyclerView();
        initCalendar();
    }

    private void initRecyclerView(){

        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        todayDate = simpleDateFormat.format(calendar.getInstance().getTime());

        //Check if there are events today
        readEvents(todayDate, events);

        //Make the Scrollview to be on the top at the launch of the activity
        recyclerView.setFocusable(false);

        //Layout manager for the recycler view
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Adapter for the recycler view
        adapter = new RecyclerViewAdapter(events, this, todayDate);
        recyclerView.setAdapter(adapter);
    }

    private void initCalendar(){
        //Set the selected date to the current date
        calendarView.setSelectedDate(calendar.getInstance());

        //By visiting each file from the internal storage, we can determine if one or multiple events have been defined for one day (contains in the filename)
        //The filenames are in this format : calendar-module-yyyy-MM-dd.txt
        for(int i = 0; i < eventsDates.length; i++){
            if(eventsDates[i].contains("calendar-module")){
                year = eventsDates[i].split("-")[2];
                month = eventsDates[i].split("-")[3];
                day = eventsDates[i].split("-")[4].substring(0,2);

                String dateString = year + "-" + month + "-"+ day;
                simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    date = simpleDateFormat.parse(dateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                //Add decorator on the calendar day in the case of one or multiple events
                dayDecorator = new DayDecorator();
                dayDecorator.setDate(date);
                calendarView.addDecorators(dayDecorator);
            }
        }
    }

    private String treatDate(CalendarDay calendarDay){
        String dateNonTreated = calendarDay.toString().split("\\{")[1];
        String dateTreated = dateNonTreated.substring(0, dateNonTreated.length() - 1);

        String yearTreated = dateTreated.substring(0,5);

        //Cases of the month is < 10
        String monthTreated = dateTreated.substring(5,7);
        if(monthTreated.contains("-")){
            //If the month is not october
            if(!monthTreated.contains("9")){
                monthTreated = "0" + String.valueOf(Integer.parseInt(dateTreated.substring(5,6)) + 1);
            }else{
                monthTreated = String.valueOf(Integer.parseInt(dateTreated.substring(5,6)) + 1);
            }
        }else{
            monthTreated = String.valueOf(Integer.parseInt(monthTreated) + 1);
        }

        String dayTreated =  dateTreated.substring(dateTreated.length()-2, dateTreated.length());
        if(dayTreated.contains("-")){
            dayTreated = dayTreated.substring(0,1) + "0" + dayTreated.substring(1,2);
        }else{
            dayTreated = "-" + dayTreated;
        }

        dateTreated = yearTreated + monthTreated + dayTreated;

        return dateTreated;
    }

    private void readEvents(String dateSelected, ArrayList<String> events){
        events.clear();
        //For all the files contained in the internal storage
        for(int i = 0; i< eventsDates.length; i++){

            //If a file has the name in the format : calendar-module-yyyy-MM-dd where yyyy-MM-dd is the selected date
            if(eventsDates[i].contains("calendar-module-" + dateSelected)){
                //Read the file and store the events in the events array
                try {

                    fileInputStream = openFileInput(eventsDates[i]);
                    inputStreamReader = new InputStreamReader(fileInputStream);
                    bufferedReader = new BufferedReader(inputStreamReader);
                    stringBuilder = new StringBuilder();

                    while ((eventLine = bufferedReader.readLine()) != null) {
                        events.add(eventLine);
                        stringBuilder.append(eventLine);

                    }
                    sortEvents(events);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        populateEvents();
    }

    private void sortEvents(ArrayList<String> events){
        Collections.sort(events);
    }

    private void populateEvents(){
        adapter = new RecyclerViewAdapter(events, this, selectedDate);
        recyclerView.setAdapter(adapter);
    }
}
