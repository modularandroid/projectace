package com.mecreativestudio.calendar_view_module;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.viewHolder> {

    private ArrayList<String> events;
    private Context context;
    private String dateReceived;
    private String grandTime;
    private String startTime;
    private String endTime;

    //Reference tot the view for each file content
    public static class viewHolder extends RecyclerView.ViewHolder{
        public TextView timeView;
        public TextView titleView;
        public TextView descriptionView;
        public viewHolder(View itemView){
            super(itemView);
            this.timeView = itemView.findViewById(R.id.timeView);
            this.titleView = itemView.findViewById(R.id.titleView);
            this.descriptionView = itemView.findViewById(R.id.descriptionView);
        }
    }

    //Constructor
    public RecyclerViewAdapter(ArrayList<String> events, Context context, String dateReceived){
        this.events = events;
        this.context = context;
        this.dateReceived = dateReceived;
    }

    //Create new views (invoked by the layout manager)
    @Override
    public RecyclerViewAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item, parent, false);
        viewHolder vH = new viewHolder(listItem);
        return vH;
    }

    //Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final viewHolder holder, int position) {
        grandTime = events.get(position).split(":::")[0];
        startTime = grandTime.split("-")[0];
        endTime = grandTime.split("-")[1];
        grandTime = startTime + "-" + endTime;

        holder.timeView.setText(grandTime);
        holder.titleView.setText(events.get(position).split(":::")[1]);
        holder.descriptionView.setText(events.get(position).split(":::")[2]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EditEvent.class);
                intent.putExtra("isExistingEvent", true);
                intent.putExtra("dateReceived", dateReceived);
                intent.putExtra("timeReceived", holder.timeView.getText().toString());
                intent.putExtra("titleReceived", holder.titleView.getText().toString());
                intent.putExtra("descriptionReceived", holder.descriptionView.getText().toString());
                context.startActivity(intent);
            }
        });

    }

    //Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return events.size();
    }
}
