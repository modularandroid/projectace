package com.mecreativestudio.calendar_view_module;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jzxiang.pickerview.TimePickerDialog;
import com.jzxiang.pickerview.data.Type;
import com.jzxiang.pickerview.listener.OnDateSetListener;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EditEvent extends AppCompatActivity implements OnDateSetListener {

    TimePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    SimpleDateFormat simpleDateFormat;

    private boolean isDate = false;
    private boolean isStartTime = false;
    private boolean isEndTime = false;

    private Boolean createEvent;
    private String dateReceived;
    private String timeReceived;
    private String titleReceived;
    private String descriptionReceived;
    private Boolean isExistingEvent;

    private TextView cancel;
    private TextView ok;
    private TextView dateInput;
    private TextView startTimeInput;
    private TextView endTimeInput;
    private EditText titleInput;
    private EditText descriptionInput;
    private TextView deleteEvent;
    private Boolean deleteEventHasBeenClicked = false;

    private String grandTime;
    private int startTimeToInt;
    private int endTimeToInt;

    private String[] eventsDates;
    private Boolean fileFound;
    private String filename;
    private String fileContent;
    private FileOutputStream outputStream;

    private FileInputStream fileInputStream;
    private InputStreamReader inputStreamReader;
    private BufferedReader bufferedReader;
    private StringBuilder stringBuilder;
    private String eventLine;
    private String eventToRemove;
    private ArrayList<String> newFileContent = new ArrayList<>();

    private int numberOfLinesInFile;

    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_event);

        //Get the internal files list of the internal storage
        eventsDates = fileList();

        //Get intent for CalendarViewActivity
        intent = getIntent();

        createEvent = intent.getBooleanExtra("createEvent", false);
        deleteEvent = findViewById(R.id.deleteEvent);
        if (createEvent) {
            deleteEvent.setVisibility(View.INVISIBLE);
        }

        dateInput = findViewById(R.id.dateInput);
        dateReceived = intent.getStringExtra("dateReceived");
        dateInput.setText(dateReceived);

        titleInput = findViewById(R.id.titleInput);
        startTimeInput = findViewById(R.id.startTimeInput);
        endTimeInput = findViewById(R.id.endTimeInput);
        descriptionInput = findViewById(R.id.descriptionInput);

        titleReceived = intent.getStringExtra("titleReceived");
        timeReceived = intent.getStringExtra("timeReceived");
        descriptionReceived = intent.getStringExtra("descriptionReceived");

        if (intent.hasExtra("titleReceived")) {
            if (!titleReceived.contains("No Event Defined")) {
                titleInput.setText(titleReceived);
                startTimeInput.setText(timeReceived.split("-")[0].trim());
                endTimeInput.setText(timeReceived.split("-")[1].trim());
                if(!descriptionReceived.equals("")){
                    descriptionInput.setText(descriptionReceived);
                }
            } else {
                deleteEvent.setVisibility(View.INVISIBLE);
            }
        }

        //Add a click listener to the cancel top bar button
        cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Add a click listener to the ok top bar button
        ok = findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intent.hasExtra("isExistingEvent")) {
                    isExistingEvent = intent.getBooleanExtra("isExistingEvent", false);

                    if (isExistingEvent) {
                        filename = "calendar-module-" + dateInput.getText().toString() + ".txt";
                        findFile(filename);
                        if (fileFound) {
                            deleteEvent(filename, timeReceived, titleReceived, descriptionReceived);
                        }
                    }
                }
                editFile();
            }
        });

        datePickerDialog = new TimePickerDialog.Builder()
                .setCallBack(this)
                .setCancelStringId("Cancel")
                .setSureStringId("Ok")
                .setTitleStringId("")
                .setYearText("")
                .setMonthText("")
                .setDayText("")
                .setCyclic(false)
                .setThemeColor(getResources().getColor(R.color.timepicker_dialog_bg))
                .setType(Type.YEAR_MONTH_DAY)
                .setWheelItemTextNormalColor(getResources().getColor(R.color.timetimepicker_default_text_color))
                .setWheelItemTextSelectorColor(getResources().getColor(R.color.timepicker_toolbar_bg))
                .setWheelItemTextSize(16)
                .build();

        //Add a click listener on the dateInput
        dateInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDate = true;
                datePickerDialog.show(getSupportFragmentManager(), "Date");
            }
        });

        timePickerDialog = new TimePickerDialog.Builder()
                .setCallBack(this)
                .setCancelStringId("Cancel")
                .setSureStringId("Ok")
                .setTitleStringId("")
                .setHourText("")
                .setMinuteText("")
                .setCyclic(false)
                .setThemeColor(getResources().getColor(R.color.timepicker_dialog_bg))
                .setType(Type.HOURS_MINS)
                .setWheelItemTextNormalColor(getResources().getColor(R.color.timetimepicker_default_text_color))
                .setWheelItemTextSelectorColor(getResources().getColor(R.color.timepicker_toolbar_bg))
                .setWheelItemTextSize(16)
                .build();

        //Add a click listener on the startTimeInput
        startTimeInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStartTime = true;
                timePickerDialog.show(getSupportFragmentManager(), "Time");
            }
        });

        //Add a click listener on the endTimeInput
        endTimeInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEndTime = true;
                timePickerDialog.show(getSupportFragmentManager(), "Time");
            }
        });

        //Add a click listener on the deleteEvent
        deleteEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteEventHasBeenClicked = true;
                filename = "calendar-module-" + dateInput.getText().toString() + ".txt";
                findFile(filename);
                if (fileFound) {
                    deleteEvent(filename, timeReceived, titleReceived, descriptionReceived);
                }
            }
        });
    }

    @Override
    public void onDateSet(TimePickerDialog timePickerDialog, long millseconds) {
        if (isDate) {
            String text = getDateToString(millseconds);
            dateInput.setText(text);
            isDate = false;
        } else if (isStartTime) {
            String text = getTimeToString(millseconds);
            startTimeInput.setText(text);
            isStartTime = false;
        } else if (isEndTime){
            String text = getTimeToString(millseconds);
            endTimeInput.setText(text);
            isEndTime = false;
        }
    }

    //Convert selected date to yyyy-MM-dd format
    public String getDateToString(long time) {
        Date date = new Date(time);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(date);
    }

    //Convert selected time to HH : mm format
    public String getTimeToString(long time) {
        Date date = new Date(time);
        simpleDateFormat = new SimpleDateFormat("HH : mm");
        return simpleDateFormat.format(date);
    }

    //Precheck before creating or editing existing file
    private void editFile() {
        if (dateInput.getText().length() > 0) {
            filename = "calendar-module-" + dateInput.getText().toString() + ".txt";

            if(startTimeInput.getText().length() > 0){
                if(endTimeInput.getText().length() > 0){

                    startTimeToInt = Integer.parseInt(startTimeInput.getText().toString().substring(0, 2).trim() + startTimeInput.getText().toString().substring(5, 7).trim());
                    endTimeToInt = Integer.parseInt(endTimeInput.getText().toString().substring(0, 2).trim() + endTimeInput.getText().toString().substring(5, 7).trim());

                    if(startTimeToInt < endTimeToInt) {
                        grandTime = startTimeInput.getText() + " - " + endTimeInput.getText();

                        findFile(filename);
                        if (fileFound) {
                            editExistingFile(grandTime, titleInput.getText().toString(), descriptionInput.getText().toString());
                        } else {
                            createFile(grandTime, titleInput.getText().toString(), descriptionInput.getText().toString());
                        }
                    }else{
                        Toast.makeText(this, "Start time must be earlier than End time", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this, "Please provide a end time", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(this, "Please provide a start time", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please provide a date", Toast.LENGTH_SHORT).show();
        }
    }

    private void findFile(String filename) {
        fileFound = false;
        //For all the files contained in the internal storage
        for (int i = 0; i < eventsDates.length; i++) {
            //If a file has the name in the format : calendar-module-yyyy-MM-dd where yyyy-MM-dd is the selected date
            if (eventsDates[i].contains(filename)) {
                fileFound = true;
            }
        }
    }

    private void editExistingFile(String time, String title, String description) {
        if (time.length() > 0) {
            if (title.length() > 0) {
                if (description.trim().length() == 0) {
                    fileContent = time + ":::" + title + "::: \n";
                } else {
                    fileContent = time + ":::" + title + ":::" + description + "\n";
                }
                try {
                    outputStream = openFileOutput(filename, Context.MODE_APPEND);
                    outputStream.write(fileContent.getBytes());
                    outputStream.close();

                    Toast.makeText(this, "Event successfully edited", Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();

            } else {
                Toast.makeText(this, "Please provide a title", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please provide a time", Toast.LENGTH_SHORT).show();
        }
    }

    private void createFile(String time, String title, String description) {
        if (time.length() > 0) {
            if (title.length() > 0) {
                if (description.trim().length() == 0) {
                    fileContent = time + ":::" + title + "::: \n";
                } else {
                    fileContent = time + ":::" + title + ":::" + description + "\n";
                }
                try {
                    outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
                    outputStream.write(fileContent.getBytes());
                    outputStream.close();

                    Toast.makeText(this, "Event successfully edited", Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();

            } else {
                Toast.makeText(this, "Please provide a title", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please provide a time", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteEvent(String filename, String time, String title, String description) {

        eventToRemove = time + ":::" + title + ":::" + description;

        try {

            //Read the  content of ile
            fileInputStream = openFileInput(filename);
            inputStreamReader = new InputStreamReader(fileInputStream);
            bufferedReader = new BufferedReader(inputStreamReader);
            stringBuilder = new StringBuilder();

            //While there are lines in the file
            while ((eventLine = bufferedReader.readLine()) != null) {
                //Check if the current line is the event to remove
                //If it is not, save the others events in the newFileContent ArrayList
                if (!eventLine.contains(eventToRemove)) {
                    newFileContent.add(eventLine + "\n");
                }
                stringBuilder.append(eventLine);
            }

            bufferedReader.close();
            inputStreamReader.close();
            fileInputStream.close();

            //Write the new content of the file
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            for (int i = 0; i < newFileContent.size(); i++) {
                outputStream.write(newFileContent.get(i).getBytes());
            }
            outputStream.close();

            //Check if the file is empty
            if(deleteEventHasBeenClicked){
                checkIfFileIsEmpty(filename);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkIfFileIsEmpty(String filename) {

        numberOfLinesInFile = 0;

        try {

            //Read the  content of ile
            fileInputStream = openFileInput(filename);
            inputStreamReader = new InputStreamReader(fileInputStream);
            bufferedReader = new BufferedReader(inputStreamReader);
            stringBuilder = new StringBuilder();

            //While there are lines in the file
            while ((eventLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(eventLine);
                numberOfLinesInFile++;
            }

            bufferedReader.close();
            inputStreamReader.close();
            fileInputStream.close();

            //If the file is empty
            if(numberOfLinesInFile == 0){
                deleteFile(filename);
            }

            Toast.makeText(this, "Event successfully deleted", Toast.LENGTH_SHORT).show();
            finish();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
