# Calendar View Module

A configurable and customizable Calendar View module, with events management, for android.



## Installation

From Android Studio, import the library module to your project :

  1. Click File > New > Import Module

  2. Choose **calendar-view-module** directory and click Finish. 
     *In case of any import error, just proceed to step 3 and the error should be automatically solved.*

  3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

	```
	include ":app", ":calendar-view-module"   
	```

  4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

	```
	dependencies {
		implementation project(":calendar-view-module")
	}
	```

  5. Click Sync Project with Gradle Files (Sync Now)

  6. In your **app AndroidManifest.xml**,  under application, add this line : 

	```
	<application
		...>
		<activity android:name="com.mecreativestudio.calendar_view_module.CalendarViewActivity"/>
	</application>
	```



## Usage

You can call the module with this simple intent :

```java
Intent intent = new Intent(YOUR_ACTIVITY_NAME.this, CalendarViewActivity.class);
startActivity(intent);
```

*Note that you must change YOUR_ACTIVITY_NAME by the name of the activity that calls the module.*



### 	Example

Let's say you have an activity called ***MainActivity*** in which a ***calendarViewButton*** button is implemented. You want your calendar view to be displayed after clicking this button.

By following the previous instructions, the code which you should have to do so is the following : 

```JAVA
import ...
import com.mecreativestudio.calendar_view_module.CalendarViewActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Add a click listener on the calendarViewButton
        Button calendarViewButton = findViewById(R.id.calendarViewButton);
        calendarViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the calendar-view-module
                Intent intent = new Intent(MainActivity.this, CalendarViewActivity.class);
                startActivity(intent);
            }
        });
    }
}
```



## Configuration and Customization

This Calendar View Module is fully configurable and customizable.  

If you want to adapt its behavior to your needs, you can simply edit the **CalendarViewActivity**, **EditEvent** and **RecyclerViewAdapter** Java classes which are located here :

```
YOUR_PROJECT/calendar-view-module/java/com.mecreativestudio.calendar_view_module/CalendarViewActivity.java
YOUR_PROJECT/calendar-view-module/java/com.mecreativestudio.calendar_view_module/EditEvent.java
YOUR_PROJECT/gridview-module/java/com.mecreativestudio.gridview_module/RecyclerViewAdapter.java
```

*Note that if you modify one of these classes, you should probably ensure overall consistency in the other two classes.*



If you want to edit its layout, you can simply edit the **calendar_view_layout.xml** and **edit_event.xml** files which are located here :

```
YOUR_PROJECT/calendar-view-module/res/layout/calendar_view_layout.xml
YOUR_PROJECT/calendar-view-module/res/layout/edit_event.xml
```



If you want to edit the layout of a single event, you can simply edit the **list_item.xml** file which is located here :

```
YOUR_PROJECT/calendar-view-module/res/layout/list_item.xml
```
