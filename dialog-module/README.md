# Dialog Module

A configurable and customizable Dialog module for android.



## Installation

From Android Studio, import the library module to your project :

  1. Click File > New > Import Module

  2. Choose **dialog-module** directory and click Finish. 
     *In case of any import error, just proceed to step 3 and the error should be automatically solved.*

  3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

	```
	include ":app", ":dialog-module"   
	```

  4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

  ```
  dependencies {
  	implementation project(":dialog-module")
  }
  ```

  5. Click Sync Project with Gradle Files (Sync Now)



## Usage

In the activity in which you want to call the module, import it by adding the following line :

```java
import com.mecreativestudio.dialog_module.DialogModule;
```



Then you can simply call it as follows :

```java
//Get the fragment manager
FragmentManager fragmentManager = getSupportFragmentManager();

//Create a new instance of dialogModule
DialogModule dialogModule = new DialogModule();

//Avoid user to exit dialog module by clicking outside of it
dialogModule.setCancelable(false);

//Show dialog module
dialogModule.show(fragmentManager, "dialogModule");
```



### 	Example

In your application, you have an activity called ***MainActivity*** that contains a button whose id is ***dialogButton***. You want to display a dialog when a click is made on this button.

By following the previous instructions, the code which you should have in your MainActivity.java class is :

```JAVA
import ...
import com.mecreativestudio.dialog_module.DialogModule;

public class MainActivity extends AppCompatActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Add a click listener on the dialogButton
        Button dialogButton = findViewById(R.id.dialogButton);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getSupportFragmentManager();
                DialogModule dialogModule = new DialogModule();
                dialogModule.setCancelable(false);
                dialogModule.show(fragmentManager, "dialogModule");
                
            }
        });
        
    }
}
```



## Configuration and Customization

This Dialog Module is fully configurable and customizable.  

If you want to adapt its behavior to your needs, you can simply edit the DialogModule Java class which is located here :

```
YOUR_PROJECT/dialog-module/java/com.mecreativestudio.dialog_module/DialogModule.java
```



If you want to edit its layout, you can simply edit the dialog_layout.xml file which is located here :

```xml
YOUR_PROJECT/dialog-module/res/layout/dialog_layout.xml
```
