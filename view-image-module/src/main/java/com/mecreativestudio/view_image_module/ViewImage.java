package com.mecreativestudio.view_image_module;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import java.io.File;

public class ViewImage extends AppCompatActivity {
    String imageLink;
    File imageFile;
    PhotoView imageToDisplayImg;
    LinearLayout imageDisplayLinearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_image);

        imageDisplayLinearLayout = findViewById(R.id.imageDisplayLinearLayout);
        imageToDisplayImg = findViewById(R.id.imageToDisplayImg);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imageDisplayLinearLayout.setOrientation(LinearLayout.VERTICAL);
        }
        else {
            imageDisplayLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        }


        Intent myIntent = getIntent();
        //If the intent contains both image url and image path
        if(myIntent.hasExtra("file") && myIntent.hasExtra("url")){
            Log.e("INTENT", "You must choose between image url or image file");
        }else{

            //If the intent contains only image url
            if(myIntent.hasExtra("url")){

                imageLink = myIntent.getStringExtra("url");
                loadImageFromUrl(imageToDisplayImg,imageLink);

            //If the intent contains only image file
            }else if(myIntent.hasExtra("file")){

                imageFile = (File)myIntent.getSerializableExtra("file");
                loadImageFromFile(imageToDisplayImg, imageFile);

            //If the intent contains nothing or something different
            }else{
                Log.e("INTENT", "No or wrong intent has been specified");
            }
        }
    }

    //Load image into the Photoview thanks to its URL
    public void loadImageFromUrl(PhotoView target, String url){
        Picasso.with(ViewImage.this)
            .load(url)
            .error(R.color.colorPrimaryDark)
            .into(target, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {

                }
            }
        );
    }

    //Load image file into the Photoview
    public void loadImageFromFile(PhotoView target, File file){
        Picasso.with(ViewImage.this)
            .load(file)
            .error(R.color.colorPrimaryDark)
            .into(target, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                }
        );
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // landscape
            imageDisplayLinearLayout.setOrientation(LinearLayout.VERTICAL);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            //  portrait
            imageDisplayLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        }
    }
}
