# View Image Module [DEPRECIATED]

A configurable and customizable Image Viewer module for android.

*This module works but is depreciated. Use instead the **View Multiple Images Module** which allows you to display one or multiple images.* 



## Requirements

You must add the maven repository to your project. To do so, from Android Studio in your **root gradle file, **add the following line: 

```java
allprojects {
     repositories {
		...
        maven { url "https://jitpack.io" }
	}
}
```



## Installation

From Android Studio, import the library module to your project :

  1. Click File > New > Import Module

  2. Choose **view-image-module** directory and click Finish.
     *In case of any import error, just proceed to step 3 and it should be automatically solved.*

  3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

```java
include ":app", ":view-image-module"
```
  4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

```java
dependencies {
    implementation project(":view-image-module")
}
```

  5. Click Sync Project with Gradle Files (Sync Now)  

  6. In your **app AndroidManifest.xml**,  under application, add this line : 

```java
<application
	...>
   	<activity android:name="com.mecreativestudio.view_image_module.ViewImage" />
</application>
```



## Usage

In the activity in which you want to call the module, import it by adding the following line :

```java
import com.mecreativestudio.view_image_module.ViewImage;
```



- If you want to display an image from its URL, you can call the module with this simple intent :


```java
Intent intent = new Intent(YOUR_ACTIVITY_NAME.this, ViewImage.class);
intent.putExtra("url", "IMAGE_URL");
startActivity(intent);
```

*Note that you must change YOUR_ACTIVITY_NAME by the name of the activity that calls the module. Of course, you must also change IMAGE_URL.*



- If you want to display an image from its path, you can call the module with this simple intent :

```java
Intent intent = new Intent(YOUR_ACTIVITY_NAME.this, ViewImage.class);
intent.putExtra("file", new File("IMAGE_PATH"));
startActivity(intent);
```

*In this case, you must change  YOUR_ACTIVITY_NAME and IMAGE_PATH.*



### 	Example

Let's say that your application contains an activity, called ***MainActivity***, in which a ***viewImageButton*** button is implemented.
You want to display an image, from its URL, with the Image Viewer Module when you click on this button. 

By following the previous instructions, the code which you should have to do so is the following : 

```java
import ...
import com.mecreativestudio.view_image_module.ViewImage;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Add a click listener on the viewImageButton
        Button viewImageButton = findViewById(R.id.viewImageButton);
        viewImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                //When a click is made on this button, open the ViewImage Module
                Intent intent = new Intent(MainActivity.this, ViewImage.class);
                intent.putExtra("url", "https://images.pexels.com/photos/1324500/pexels-photo-1324500.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260");
                startActivity(intent);
                
            }
        });
    }
}
```



## Configuration and Customization

This Image Viewer Module is fully configurable and customizable.  

If you want to adapt its behavior to your needs, you can simply edit the ViewImage Java class which is located here :

```java
YOUR_PROJECT/view-image-module/java/com.mecreativestudio.view_image_module/ViewImage.java
```



If you want to adapt its layout to your needs, you can simply edit the view_image.xml file which is located here :

```java
YOUR_PROJECT/view-image-module/res/layout/view_image.xml
```