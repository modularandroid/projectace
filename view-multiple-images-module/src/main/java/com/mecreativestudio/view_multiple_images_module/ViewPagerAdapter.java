package com.mecreativestudio.view_multiple_images_module;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<String> imageUrls;
    private ArrayList<File> imageFiles;
    private LayoutInflater layoutInflater;

    public ViewPagerAdapter(Context context, ArrayList<String> imageUrls, ArrayList<File> imageFiles) {
        this.context = context;
        this.imageUrls = imageUrls;
        this.imageFiles = imageFiles;
    }

    @Override
    public int getCount() {
        if(imageUrls != null){
            return imageUrls.size();
        }else if(imageFiles != null){
            return imageFiles.size();
        }else{
            Log.e("VIEW PAGER ADAPTER", "Something went wrong while getting size of arrayList");
            return 0;
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_layout, null);
        PhotoView photoView = view.findViewById(R.id.photoView);

        if(imageUrls != null){
            loadImageFromUrl(photoView, imageUrls.get(position));
        }else if(imageFiles != null){
            loadImageFromFile(photoView, imageFiles.get(position));
        }else{
            Log.e("VIEW PAGER ADAPTER", "No arrayList has been defined");
        }

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    //Load image into the Photoview with its URL
    private void loadImageFromUrl(PhotoView target, String url){
        Picasso.with(context)
            .load(url)
            .error(R.color.colorPrimaryDark)
            .into(target, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Log.e("PICASSO", "Something went wrong while loading image from its URL");
                }
            }
        );
    }

    //Load image file into the Photoview with its associated File
    private void loadImageFromFile(PhotoView target, File file){
        Picasso.with(context)
            .load(file)
            .error(R.color.colorPrimaryDark)
            .into(target, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Log.e("PICASSO", "Something went wrong while loading image from its associated file");
                    }
                }
            );
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}