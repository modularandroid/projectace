# Select Image Module

A configurable and customizable Image Selector module for android.



## Requirements

You must add the maven repository to your project. To do so, from Android Studio in your **root gradle file, **add the following line: 

```java
allprojects {
     repositories {
		...
        maven { url "https://jitpack.io" }
	}
}
```



## Installation

From Android Studio, import the library module to your project :

  1. Click File > New > Import Module

  2. Choose **select-image-module** directory and click Finish.
     *In case of any import error, just proceed to step 3 and it should be automatically solved.*

  3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

```java
include ":app", ":select-image-module"
```

4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

```java
dependencies {
    implementation project(":select-image-module")
}
```

5. Click Sync Project with Gradle Files (Sync Now)

6. In your **app AndroidManifest.xml**, add the following permissions :

```java
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.permission.VIBRATE" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-feature android:name="android.hardware.camera" />
<uses-feature android:name="android.hardware.camera.autofocus" android:required="false" />
```

   In the same file, under application, add this line : 

```java
<application
	...>
   	<activity android:name="com.mecreativestudio.select_image_module.SelectImage" />
</application>
```



## Usage

In the activity in which you want to call the module, import it by adding the following line :

```java
import com.mecreativestudio.select_image_module.SelectImage;
```



Before any call to this module, it is essential that the camera permissions are granted. In order to verify this, you can use the following code : 

```java
//Check for camera permission
if (ContextCompat.checkSelfPermission(YOUR_ACTIVITY_NAME.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

	//Request for granted the camera permission
	ActivityCompat.requestPermissions(YOUR_ACTIVITY_NAME.this, new String[{Manifest.permission.CAMERA}, 102);
	
}
```

*Note that you must change YOUR_ACTIVITY_NAME by the name of the activity that calls the module.*



When camera permissions are granted, you can call this module with a simple intent :

```java
Intent intent = new Intent(YOUR_ACTIVITY_NAME.this, SelectImage.class);
startActivity(intent);
```



After an, or multiple, image has been selected and the user has clicked on the validate button, you can specificy the action you want to do in the SelectImage.java class *(cf. Configuration and Customization)*, and especially in this part of the code :

```java
.onResult(new Action<ArrayList<AlbumFile>>() {
    @Override
    public void onAction(@NonNull ArrayList<AlbumFile> result) {
        mAlbumFiles = result;
        
        //Get the path for each image and store it in the imgPaths strings arrayList
        for (int i=0; i<mAlbumFiles.size(); i++){
            imgPaths.add(mAlbumFiles.get(i).getPath());
        }
        
        //DO SOMETHING HERE
    }
})
```

*Note that the SelectImage.java class contains a commented code, at the "DO SOMETHING HERE" level, to pass the paths of the selected images to another activity.*



### 	Example

Let's say that your application contains an activity, called ***MainActivity***, in which a ***selectImageButton*** button is implemented.
You want the Select Image Module to be called when you click on this button. 

By following the previous instructions, the code which you should have to do so is the following : 

```java
import ...
import com.mecreativestudio.select_image_module.SelectImage;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Check for camera permission
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //Request for granted the camera permission
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 102);
        }

        //Add a click listener on the selectImageButton
        Button selectImageButton = findViewById(R.id.selectImageButton);
        selectImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the Select Image Module
                Intent intent = new Intent(MainActivity.this, SelectImage.class);
                startActivity(intent);
            }
        });
    }
}
```



## Configuration and Customization

This Select Image Module is fully configurable and customizable.  

If you want to adapt its behavior and appearance to your needs, you can simply edit the SelectImage Java class which is located here :

```java
YOUR_PROJECT/select-image-module/java/com.mecreativestudio.select_image_module/SelectImage.java
```

*Note that all the configuration options are available here : https://github.com/yanzhenjie/Album*
*This library also allows you to work with videos*