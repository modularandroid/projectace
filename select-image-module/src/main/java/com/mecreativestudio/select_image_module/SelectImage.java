package com.mecreativestudio.select_image_module;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.yanzhenjie.album.Action;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.api.widget.Widget;

import java.util.ArrayList;
import java.util.Locale;

public class SelectImage extends AppCompatActivity {

    private ArrayList<AlbumFile> mAlbumFiles;
    private ArrayList<String> imgPaths = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Album.initialize(AlbumConfig.newBuilder(this)
                .setAlbumLoader(new MediaLoader())
                .setLocale(Locale.getDefault())
                .build()
        );
    }

    @Override
    protected void onStart() {
        super.onStart();

        imgPaths.clear();
        selectImage();
    }

    private void selectImage() {

        /*All the options are available here : https://github.com/yanzhenjie/Album */

        Album.image(this)
            //Allow multiple choice
            .multipleChoice()
            //Whether the camera appears or not
            .camera(true)
            //Number of displayed columns
            .columnCount(2)
            //Maximum of images that can be selected
            .selectCount(10)
            //Reverse the list
            .checkedList(mAlbumFiles)
            .widget(
                Widget.newDarkBuilder(this)
                    .title("Select Image")
                    .build()
            )
            .onResult(new Action<ArrayList<AlbumFile>>() {
                @Override
                public void onAction(@NonNull ArrayList<AlbumFile> result) {
                    mAlbumFiles = result;

                    //Get the path of each image and store it in the imgPaths arrayList
                    for (int i=0; i<mAlbumFiles.size(); i++){
                        imgPaths.add(mAlbumFiles.get(i).getPath());
                    }

                    //DO SOMETHING

                /*-------------------------------------------------------------------------------------------------------------------------*/
                /*------------------------------------------PASS IMG PATHS TO ANOTHER ACTIVITY---------------------------------------------*/
                /*-------------------------------------------------------------------------------------------------------------------------*/

                    /*Pass the images paths (arrayList of strings) to another activity (YOUR_ACTIVITY.class) if you need to*/
                    //Intent intent = new Intent(SelectImage.this,YOUR_ACTIVITY.class);
                    //intent.putStringArrayListExtra("imgPaths", imgPaths);
                    //startActivity(intent);


                    /*Fetch the images paths in YOUR_ACTIVITY.class*/
                    /*The line below must be paste in YOUR_ACTIVITY.class (to get the image paths) and shouldn't exist in this file */
                    //ArrayList<String> imgPaths=getIntent().getExtras().getStringArrayList("imgPaths");

                /*-------------------------------------------------------------------------------------------------------------------------*/
                /*-------------------------------------------------------------------------------------------------------------------------*/
                /*-------------------------------------------------------------------------------------------------------------------------*/
                }
            })
            .onCancel(new Action<String>() {
                @Override
                public void onAction(@NonNull String result) {
                    Toast.makeText(SelectImage.this, "Canceled", Toast.LENGTH_LONG).show();

                    finish();

                }
            })
            .start();
    }
}
