# Navigation Drawer Tutorial

This document is intended to show and facilitate the implementation of a Navigation Drawer into an Android application.



For the purposes of this tutorial, we assume that our application contains 4 activities : ***MainActivity, MyScreen1, MyScreen2 and MyScreen3***. We decide to implement the Navigation Drawer in the MainActivity

 Of course, all the following manipulations are done from Android Studio.



## Prerequisites

Make sure your activities are listed in your ***AndroidManifest.xml***

In our case, we have: 

```xml
<manifest ...>

    <application
        ... >
        <activity android:name=".MainActivity">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
        <activity android:name=".MyScreen1"/>
        <activity android:name=".MyScreen2"/>
        <activity android:name=".MyScreen3"/>
    </application>

</manifest>
```



## Step 1 : Add the dependencies

In your ***app gradle file***, add the following 4 lines in the dependencies block :

```
dependencies {
    ...

    implementation "com.mikepenz:materialdrawer:6.0.9"
    implementation 'com.android.support:appcompat-v7:28.0.0-rc01'
    implementation 'com.android.support.constraint:constraint-layout:1.1.2'
    implementation "com.android.support:recyclerview-v7:28.0.0-rc01"
    
}

```

Then, click on ***Sync Now***



## Step 2 : Import the needed classes

In the ***MainActivity***, add the following classes to the import section :

```java
import ...
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
```



## Step 3 : Prepare the layout

This step is very important. It allows us to define a Toolbar that will be used as an entry point to the Navigation Drawer.

So in our case, in the layout ***activity_main.xml***, we have a Relative Layout containing 2 children : The Toolbar and a ScrollView (itself containing children item1, item2, ...).

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity"
    android:orientation="horizontal">

    <android.support.v7.widget.Toolbar
        android:id="@+id/myToolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="8dp" />

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
       	...>

            <item1
                ... />
       
        	<item2
                ... />
        
    </ScrollView>

</RelativeLayout>
```

*Note that adding a Toolbar can be optional here as the Navigation Drawer opens by swiping from left to right and closes by swiping in the opposite direction*



## Step 4 : Initialize the Navigation Drawer

In the ***MainActivity***, add the following lines in the overrided onCreate() method :

```java
import ...
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //Find and bring the Toolbar to the front
        Toolbar myToolbar = findViewById(R.id.myToolbar);
        myToolbar.bringToFront();
        
        //The Android Original Action Bar is now our Toolbar
        setSupportActionBar(myToolbar);
        //The title of the Toolbar is empty
        getSupportActionBar().setTitle(" ");

        //Call the Navigation Drawer Builder method
        NavigationDrawerModule(this, myToolbar);
 
    }
}
```



## Step 5 : Define the Navigation Drawer Builder Method

Finally, in the ***MainActivity***, all you have to do is define your Navigation Drawer Builder method as follows :

```java
import ...
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
		...
    }
    
    private void NavigationDrawerBuilder(Context context, Toolbar toolbar){

        final Activity activity = (Activity) context;

        //Define the items inside the drawer
        PrimaryDrawerItem myScreenItem1 = new PrimaryDrawerItem().withIdentifier(1).withName("My Screen 1");
        PrimaryDrawerItem myScreenItem2 = new PrimaryDrawerItem().withIdentifier(2).withName("My Screen 2");
        PrimaryDrawerItem myScreenItem3 = new PrimaryDrawerItem().withIdentifier(3).withName("My Screen 3");

        //Create the Header
        AccountHeader drawerHeader = new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.YOUR_DRAWABLE_FOR_THE_HEADER)
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withAccountHeader(drawerHeader)
                .withActivity(activity)
                .withToolbar(toolbar)
                .addDrawerItems(
                        myScreenItem1,
                        new DividerDrawerItem(),
                        myScreenItem2,
                        new DividerDrawerItem(),
                        myScreenItem3

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        switch ((int) drawerItem.getIdentifier()){
                            case 1:
                                startActivity(new Intent(activity, MyScreen1.class));
                                return true;
                            case 2:
                                startActivity(new Intent(activity, MyScreen2.class));
                                return true;
                            case 3:
                                startActivity(new Intent(activity, MyScreen3.class));
                                return true;
                        }
                        return true;
                    }
                })
                .build();
    }
}
```

*Note that you must change YOUR_DRAWABLE_FOR_THE_HEADER to its current name in your app.*



- Here, we built a Navigation Drawer, called by a click on the Toolbar, containing a header and 3 items: My Screen 1, My Screen 2 and My Screen 3.
- Each of these items is separated by a horizontal bar.
- When clicking on one of these items, we redirect to the associated screen.



In conclusion, we have implemented a Navigation Drawer in our MainActivity. It is possible to do this for all activities in which you want to implement a Navigation Drawer, by repeating and adapting steps 2 to 5.



Also, I invite you to get and run the Project Ace folder present in the Bitbucket ModularAndroid Group. This contains all the modules developed so far and an implementation example of Navigation Drawer.



## Configuration and Customization

If you want to know all the configuration and customization options, I invite you to go here : https://github.com/mikepenz/MaterialDrawer